let arr = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];

let parent = document.body;
function getList(arr, parent) {
  let ul = document.createElement("ul");
  for (const item of arr) {
    let li = document.createElement("li");

    if (Array.isArray(item)) {
      let ol = document.createElement("ol");
      for (const subItem of item) {
        let subLi = document.createElement("li");

        subLi.append(subItem);
        ol.append(subLi);
        ul.appendChild(ol);
      }
    } else {
      console.log(item);
      li.append(item);
      ul.append(li);
      parent.append(ul);
    }
  }
}
getList(arr, parent);
